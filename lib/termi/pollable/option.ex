defmodule Termi.Pollable.Option do
  use Ecto.Schema
  import Ecto.Changeset
  alias Termi.Votable.Vote

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "options" do
    field :datetime, :utc_datetime
    field :poll_id, :binary_id
    field :deleted, :boolean,  virtual: true
    has_many :votes, Vote

    timestamps()
  end

  @doc false
  def changeset(option, attrs) do
    option
    |> cast(attrs, [:datetime])
    |> cast_assoc(:votes, with: &Vote.changeset/2)
    |> validate_required([:datetime])
  end
end
