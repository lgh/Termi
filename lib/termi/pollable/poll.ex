defmodule Termi.Pollable.Poll do
  use Ecto.Schema
  import Ecto.Changeset
  alias Termi.Pollable.Option

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "polls" do
    field :anonymous, Ecto.Enum, values: [:yes, :no]
    field :compulsatory, Ecto.Enum, values: [:yes, :no]
    field :description, :string
    field :duration, :integer
    field :expiration, Ecto.Enum, values: [:week, :month, :quarter, :half, :full, :never]
    field :title, :string

    has_many :options, Option

    timestamps()
  end

  @doc false
  def changeset(poll, attrs) do
    poll
    |> cast(attrs, [:title, :description, :duration, :expiration, :anonymous, :compulsatory])
    # |> cast_assoc(:options, with: &Option.changeset/2, required: true)
    |> cast_assoc(:options, with: &Option.changeset/2)
    |> validate_required([:title, :description, :duration, :expiration, :anonymous, :compulsatory])
  end
end
