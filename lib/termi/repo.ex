defmodule Termi.Repo do
  use Ecto.Repo,
    otp_app: :termi,
    adapter: Ecto.Adapters.Postgres
end
