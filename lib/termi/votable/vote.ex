defmodule Termi.Votable.Vote do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "votes" do
    field :answer, Ecto.Enum, values: [:yes, :no, :maybe]
    field :name, :string
    field :option_id, :binary_id

    timestamps()
  end

  @doc false
  def changeset(vote, attrs) do
    vote
    |> cast(attrs, [:name, :answer])
    |> validate_required([:name, :answer])
  end
end
