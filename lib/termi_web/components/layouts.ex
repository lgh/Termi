defmodule TermiWeb.Layouts do
  use TermiWeb, :html

  embed_templates "layouts/*"
end
