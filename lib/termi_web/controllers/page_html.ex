defmodule TermiWeb.PageHTML do
  use TermiWeb, :html

  embed_templates "page_html/*"
end
