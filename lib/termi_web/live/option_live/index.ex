defmodule TermiWeb.OptionLive.Index do
  use TermiWeb, :live_view

  alias Termi.Pollable
  alias Termi.Pollable.Option

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :options, Pollable.list_options())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Option")
    |> assign(:option, Pollable.get_option!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Option")
    |> assign(:option, %Option{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Options")
    |> assign(:option, nil)
  end

  @impl true
  def handle_info({TermiWeb.OptionLive.FormComponent, {:saved, option}}, socket) do
    {:noreply, stream_insert(socket, :options, option)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    option = Pollable.get_option!(id)
    {:ok, _} = Pollable.delete_option(option)

    {:noreply, stream_delete(socket, :options, option)}
  end
end
