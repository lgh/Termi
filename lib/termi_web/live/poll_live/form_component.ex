defmodule TermiWeb.PollLive.FormComponent do
  use TermiWeb, :live_component

  alias Termi.Pollable
  alias Termi.Pollable.Option

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage poll records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="poll-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:title]} type="text" label="Title" />
        <.input field={@form[:description]} type="text" label="Description" />
        <.input field={@form[:duration]} type="number" label="Duration" />
        <fieldset class="flex flex-col gap-2">
          <legend>Options</legend>
          <.inputs_for :let={o_line} field={@form[:options]}>
            <.line o_line={o_line} />
          </.inputs_for>
          <.button class="mt-2" type="button" phx-click="add-option">Add</.button>
        </fieldset>
        <.input
          field={@form[:expiration]}
          type="select"
          label="Expiration"
          prompt="Choose a value"
          options={Ecto.Enum.values(Termi.Pollable.Poll, :expiration)}
        />
        <.input
          field={@form[:anonymous]}
          type="select"
          label="Anonymous"
          prompt="Choose a value"
          options={Ecto.Enum.values(Termi.Pollable.Poll, :anonymous)}
        />
        <.input
          field={@form[:compulsatory]}
          type="select"
          label="Compulsatory"
          prompt="Choose a value"
          options={Ecto.Enum.values(Termi.Pollable.Poll, :compulsatory)}
        />
        <:actions>
          <.button phx-disable-with="Saving...">Save Poll</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  def line(assigns) do
    # TODO changeset is gone. Find a way to run the action
    #   assigns =
    #     assign(
    #       assigns,
    #       :deleted,
    #       Phoenix.HTML.Form.input_value(assigns.o_line.data, :delete) == true
    #     )

    ~H"""
    <div class={if(@o_line[:deleted], do: "opacity-50")}>
      <div class="flex gap-4 items-end">
        <div class="grow">
          <.input class="mt-0" field={@o_line[:datetime]} type="datetime-local" label="Datetime" />
        </div>
        <.button class="grow-0" type="button" phx-click="delete-line" phx-value-index={@o_line.index}>
          Delete
        </.button>
      </div>
    </div>
    """
  end

  @impl true
  def update(%{poll: poll} = assigns, socket) do
    changeset = Pollable.change_poll(poll)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"poll" => poll_params}, socket) do
    changeset =
      socket.assigns.poll
      |> Pollable.change_poll(poll_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"poll" => poll_params}, socket) do
    save_poll(socket, socket.assigns.action, poll_params)
  end

  def handle_event("add-option", %{"poll" => poll_params}, socket) do
    changeset = Pollable.change_poll(socket.assigns.poll, poll_params)
    existing = get_change_or_field(changeset, :options)

    changeset =
      changeset
      |> Ecto.Changeset.put_assoc(changeset, :options, existing ++ [%Option{}])
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("delete-line", %{"index" => index}, socket) do
    IO.puts("No implemented delete: ")
  end

  defp save_poll(socket, :edit, poll_params) do
    case Pollable.update_poll(socket.assigns.poll, poll_params) do
      {:ok, poll} ->
        notify_parent({:saved, poll})

        {:noreply,
         socket
         |> put_flash(:info, "Poll updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_poll(socket, :new, poll_params) do
    case Pollable.create_poll(poll_params) do
      {:ok, poll} ->
        notify_parent({:saved, poll})

        {:noreply,
         socket
         |> put_flash(:info, "Poll created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})

  # TODO replace with Ecto.Changeset.get_assoc on ecto 3.10
  defp get_change_or_field(changeset, field) do
    with nil <- Ecto.Changeset.get_change(changeset, field) do
      Ecto.Changeset.get_field(changeset, field, [])
    end
  end
end
