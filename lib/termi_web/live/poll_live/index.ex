defmodule TermiWeb.PollLive.Index do
  use TermiWeb, :live_view

  alias Termi.Pollable
  alias Termi.Pollable.Poll
  alias Termi.Pollable.Option

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
    stream(socket, :polls, Pollable.list_polls())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Poll")
    |> assign(:poll, Pollable.get_poll!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Poll")
    |> assign(:poll, %Poll{options: [%Option{}]})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Polls")
    |> assign(:poll, nil)
  end

  @impl true
  def handle_info({TermiWeb.PollLive.FormComponent, {:saved, poll}}, socket) do
    {:noreply, stream_insert(socket, :polls, poll)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    poll = Pollable.get_poll!(id)
    {:ok, _} = Pollable.delete_poll(poll)

    {:noreply, stream_delete(socket, :polls, poll)}
  end


  def handle_event("add-option", _params, socket) do
     changeset = Termi.Pollable.change_poll(socket.assigns.poll)
     options = Ecto.Changeset.get_field(changeset, :options)

    updated_changeset =
      Ecto.Changeset.put_assoc(changeset, :options, options ++ [Pollable.create_poll(%{deleted: false})])

    IO.inspect(socket)
    # TODO Fix changeset and form are not used any more
    socket =  assign(socket, %{form: to_form(updated_changeset), changeset: updated_changeset})

    {:noreply, socket}
  end

  def handle_event("delete-line", %{"index" => index}, socket) do
    IO.puts("No implemented delete: ")
  end

  # TODO replace with Ecto.Changeset.get_assoc on ecto 3.10
  defp get_change_or_field(changeset, field) do
    with nil <- Ecto.Changeset.get_change(changeset, field) do
      Ecto.Changeset.get_field(changeset, field, [])
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
  end
end
