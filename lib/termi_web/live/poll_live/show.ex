defmodule TermiWeb.PollLive.Show do
  use TermiWeb, :live_view

  alias Termi.Pollable
  alias Termi.Repo

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:poll, Pollable.get_poll!(id) |> Repo.preload(:options))}
  end

  defp page_title(:show), do: "Show Poll"
  defp page_title(:edit), do: "Edit Poll"
end
