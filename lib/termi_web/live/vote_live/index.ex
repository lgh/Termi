defmodule TermiWeb.VoteLive.Index do
  use TermiWeb, :live_view

  alias Termi.Votable
  alias Termi.Votable.Vote

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :votes, Votable.list_votes())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Vote")
    |> assign(:vote, Votable.get_vote!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Vote")
    |> assign(:vote, %Vote{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Votes")
    |> assign(:vote, nil)
  end

  @impl true
  def handle_info({TermiWeb.VoteLive.FormComponent, {:saved, vote}}, socket) do
    {:noreply, stream_insert(socket, :votes, vote)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    vote = Votable.get_vote!(id)
    {:ok, _} = Votable.delete_vote(vote)

    {:noreply, stream_delete(socket, :votes, vote)}
  end
end
