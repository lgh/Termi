defmodule TermiWeb.Router do
  use TermiWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {TermiWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TermiWeb do
    pipe_through :browser

    get "/", PageController, :home
    live "/polls", PollLive.Index, :index
    live "/polls/new", PollLive.Index, :new
    live "/polls/:id/edit", PollLive.Index, :edit

    live "/polls/:id", PollLive.Show, :show
    live "/polls/:id/show/edit", PollLive.Show, :edit

    ### Remove later
    live "/options", OptionLive.Index, :index
    live "/options/new", OptionLive.Index, :new
    live "/options/:id/edit", OptionLive.Index, :edit

    live "/options/:id", OptionLive.Show, :show
    live "/options/:id/show/edit", OptionLive.Show, :edit
  end

  # Other scopes may use custom stacks.
  # scope "/api", TermiWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:termi, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: TermiWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
