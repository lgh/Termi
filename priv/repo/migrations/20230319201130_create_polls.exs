defmodule Termi.Repo.Migrations.CreatePolls do
  use Ecto.Migration

  def change do
    create table(:polls, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :title, :string
      add :description, :string
      add :duration, :integer
      add :expiration, :string
      add :anonymous, :string
      add :compulsatory, :string

      timestamps()
    end
  end
end
