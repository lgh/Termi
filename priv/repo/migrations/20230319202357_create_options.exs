defmodule Termi.Repo.Migrations.CreateOptions do
  use Ecto.Migration

  def change do
    create table(:options, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :datetime, :utc_datetime
      add :poll_id, references(:polls, on_delete: :delete_all, type: :binary_id)

      timestamps()
    end

    create index(:options, [:poll_id])
  end
end
