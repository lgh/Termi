defmodule Termi.Repo.Migrations.CreateVotes do
  use Ecto.Migration

  def change do
    create table(:votes, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :answer, :string
      add :option_id, references(:options, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:votes, [:option_id])
  end
end
