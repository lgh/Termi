# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Termi.Repo.insert!(%Termi.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Termi.Pollable.Poll
alias Termi.Repo


if Mix.env() == :dev do
  poll =
    Poll.changeset(
      %Poll{},
      %{
        title: "Example Poll",
        description: "We use this Poll for testing",
        duration: 30,
        expiration: :month,
        anonymous: :yes,
        compulsatory: :no,
        options: [
          %{
            datetime: ~N[2000-01-01 23:00:07],
            votes: [%{answer: :yes, name: "Liam"}, %{answer: :yes, name: "Santiago"}]
          },
          %{
            datetime: ~N[2022-12-18 19:51:00],
            votes: [%{answer: :maybe, name: "Miguel"}, %{answer: :no, name: "Daniel"}]
          }
        ]
      }
    )

  Repo.insert!(poll)
  |> Repo.preload([options: [:votes]  ])
  |> IO.inspect()
end
