defmodule Termi.PollableFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Termi.Pollable` context.
  """

  @doc """
  Generate a poll.
  """
  def poll_fixture(attrs \\ %{}) do
    {:ok, poll} =
      attrs
      |> Enum.into(%{
        anonymous: :yes,
        compulsatory: :yes,
        description: "some description",
        duration: 42,
        expiration: :week,
        title: "some title"
      })
      |> Termi.Pollable.create_poll()

    poll
  end

  @doc """
  Generate a option.
  """
  def option_fixture(attrs \\ %{}) do
    {:ok, option} =
      attrs
      |> Enum.into(%{
        datetime: ~U[2023-03-18 20:23:00Z]
      })
      |> Termi.Pollable.create_option()

    option
  end
end
