defmodule Termi.VotableFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Termi.Votable` context.
  """

  @doc """
  Generate a vote.
  """
  def vote_fixture(attrs \\ %{}) do
    {:ok, vote} =
      attrs
      |> Enum.into(%{
        answer: :yes,
        name: "some name"
      })
      |> Termi.Votable.create_vote()

    vote
  end
end
