defmodule Termi.PollableTest do
  use Termi.DataCase

  alias Termi.Pollable

  describe "polls" do
    alias Termi.Pollable.Poll

    import Termi.PollableFixtures

    @invalid_attrs %{anonymous: nil, compulsatory: nil, description: nil, duration: nil, expiration: nil, title: nil}

    test "list_polls/0 returns all polls" do
      poll = poll_fixture()
      assert Pollable.list_polls() == [poll]
    end

    test "get_poll!/1 returns the poll with given id" do
      poll = poll_fixture()
      assert Pollable.get_poll!(poll.id) == poll
    end

    test "create_poll/1 with valid data creates a poll" do
      valid_attrs = %{anonymous: :yes, compulsatory: :yes, description: "some description", duration: 42, expiration: :week, title: "some title"}

      assert {:ok, %Poll{} = poll} = Pollable.create_poll(valid_attrs)
      assert poll.anonymous == :yes
      assert poll.compulsatory == :yes
      assert poll.description == "some description"
      assert poll.duration == 42
      assert poll.expiration == :week
      assert poll.title == "some title"
    end

    test "create_poll/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Pollable.create_poll(@invalid_attrs)
    end

    test "update_poll/2 with valid data updates the poll" do
      poll = poll_fixture()
      update_attrs = %{anonymous: :no, compulsatory: :no, description: "some updated description", duration: 43, expiration: :month, title: "some updated title"}

      assert {:ok, %Poll{} = poll} = Pollable.update_poll(poll, update_attrs)
      assert poll.anonymous == :no
      assert poll.compulsatory == :no
      assert poll.description == "some updated description"
      assert poll.duration == 43
      assert poll.expiration == :month
      assert poll.title == "some updated title"
    end

    test "update_poll/2 with invalid data returns error changeset" do
      poll = poll_fixture()
      assert {:error, %Ecto.Changeset{}} = Pollable.update_poll(poll, @invalid_attrs)
      assert poll == Pollable.get_poll!(poll.id)
    end

    test "delete_poll/1 deletes the poll" do
      poll = poll_fixture()
      assert {:ok, %Poll{}} = Pollable.delete_poll(poll)
      assert_raise Ecto.NoResultsError, fn -> Pollable.get_poll!(poll.id) end
    end

    test "change_poll/1 returns a poll changeset" do
      poll = poll_fixture()
      assert %Ecto.Changeset{} = Pollable.change_poll(poll)
    end
  end

  describe "options" do
    alias Termi.Pollable.Option

    import Termi.PollableFixtures

    @invalid_attrs %{datetime: nil}

    test "list_options/0 returns all options" do
      option = option_fixture()
      assert Pollable.list_options() == [option]
    end

    test "get_option!/1 returns the option with given id" do
      option = option_fixture()
      assert Pollable.get_option!(option.id) == option
    end

    test "create_option/1 with valid data creates a option" do
      valid_attrs = %{datetime: ~U[2023-03-18 20:23:00Z]}

      assert {:ok, %Option{} = option} = Pollable.create_option(valid_attrs)
      assert option.datetime == ~U[2023-03-18 20:23:00Z]
    end

    test "create_option/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Pollable.create_option(@invalid_attrs)
    end

    test "update_option/2 with valid data updates the option" do
      option = option_fixture()
      update_attrs = %{datetime: ~U[2023-03-19 20:23:00Z]}

      assert {:ok, %Option{} = option} = Pollable.update_option(option, update_attrs)
      assert option.datetime == ~U[2023-03-19 20:23:00Z]
    end

    test "update_option/2 with invalid data returns error changeset" do
      option = option_fixture()
      assert {:error, %Ecto.Changeset{}} = Pollable.update_option(option, @invalid_attrs)
      assert option == Pollable.get_option!(option.id)
    end

    test "delete_option/1 deletes the option" do
      option = option_fixture()
      assert {:ok, %Option{}} = Pollable.delete_option(option)
      assert_raise Ecto.NoResultsError, fn -> Pollable.get_option!(option.id) end
    end

    test "change_option/1 returns a option changeset" do
      option = option_fixture()
      assert %Ecto.Changeset{} = Pollable.change_option(option)
    end
  end
end
