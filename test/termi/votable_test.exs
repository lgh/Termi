defmodule Termi.VotableTest do
  use Termi.DataCase

  alias Termi.Votable

  describe "votes" do
    alias Termi.Votable.Vote

    import Termi.VotableFixtures

    @invalid_attrs %{answer: nil, name: nil}

    test "list_votes/0 returns all votes" do
      vote = vote_fixture()
      assert Votable.list_votes() == [vote]
    end

    test "get_vote!/1 returns the vote with given id" do
      vote = vote_fixture()
      assert Votable.get_vote!(vote.id) == vote
    end

    test "create_vote/1 with valid data creates a vote" do
      valid_attrs = %{answer: :yes, name: "some name"}

      assert {:ok, %Vote{} = vote} = Votable.create_vote(valid_attrs)
      assert vote.answer == :yes
      assert vote.name == "some name"
    end

    test "create_vote/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Votable.create_vote(@invalid_attrs)
    end

    test "update_vote/2 with valid data updates the vote" do
      vote = vote_fixture()
      update_attrs = %{answer: :no, name: "some updated name"}

      assert {:ok, %Vote{} = vote} = Votable.update_vote(vote, update_attrs)
      assert vote.answer == :no
      assert vote.name == "some updated name"
    end

    test "update_vote/2 with invalid data returns error changeset" do
      vote = vote_fixture()
      assert {:error, %Ecto.Changeset{}} = Votable.update_vote(vote, @invalid_attrs)
      assert vote == Votable.get_vote!(vote.id)
    end

    test "delete_vote/1 deletes the vote" do
      vote = vote_fixture()
      assert {:ok, %Vote{}} = Votable.delete_vote(vote)
      assert_raise Ecto.NoResultsError, fn -> Votable.get_vote!(vote.id) end
    end

    test "change_vote/1 returns a vote changeset" do
      vote = vote_fixture()
      assert %Ecto.Changeset{} = Votable.change_vote(vote)
    end
  end
end
